---
title: Fediverso
subtitle: Akkoma
icon: fa-asterisk
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - fediverso
        - akkoma
        - fe
page-toc:
    active: false
---

![](fediverse.png)
# ¿Qué es el Fediverso?

El **Fediverso** (vocablo compuesto por las palabras "federación" y "universo") es una red de servidores independientes que proveen distintos tipos de programas y servicios federados, esto es, que pueden interactuar y comunicarse entre sí sobre la base de estándares de comunicación y operación abiertos. Está formada por redes sociales, servicios de blogging, microblogging y hospedaje de archivos dedicados mayormente a la creación y publicación de contenido. Sus principales caracerísticas son su carácter descentralizado, su interoperabilidad, la predominancia de protocolos de estándares abiertos y programas con licencias libres y la ausencia de una autoridad o entidad central que la concentre y controle. A diferencia de servicios centralizados privados como **Facebook**, **Twitter** o **Instagram**, esta red permite a las personas usuarias no solo comunicarse y colaborar entre ellas sin importar el programa o el servidor que elijan sino también tener control sobre sus datos.

Si han leído nuestra [**Declaración de Principios**](https://disroot.org/es/mission-statement) (si no lo hicieron, les invitamos a hacerlo) entonces comprenderán la importancia que tiene para nosotros conceptual y culturalmente.

Dentro del Fediverso, hay una cantidad creciente de plataformas y servicios enfocados en las personas usuarias, en su autonomía y privacidad. Nosotros los hemos venido utilizando y promocionando desde el inicio mismo de **Disroot** e intentamos distintas aproximaciones. Esta vez, hemos decidido dar pasos más pequeños pero más firmes en esa dirección.

Actualmente, el protocolo [**ActivityPub**](https://www.w3.org/TR/activitypub/), es el motor principal de este Fediverso. Su rápida y extendida adopción se deben al constante desarrollo y la madurez que ha alcanzado. Como prueba, basta mirar la amplia gama de plataformas y servicios que lo implementan: **Mastodon**, **Pleroma**, **Nextcloud**, **PeerTube**, **Funkwhale**, **Friendica**, **PixelFed**, **BookWyrm**, **Lemmy**, **Plume**, **Writefreely** y **Misskey**, entre otros.

Por todo ello, y porque _"queremos incentivar el surgimiento de más proveedores independientes de servicios descentralizados que trabajen cooperativamente, y no en competencia, unos con otros"_ decidimos crear **FEDisroot**, nuestra instancia de **Akkoma**. La primera de las muchas semillas que esperamos poder plantar en este maravilloso ecosistema.

Con este nuevo proyecto, esperamos no solo brindarles una herramienta de comunicación accesible, abierta y útil, sino también colaborar en la construcción de _"una red descentralizada verdaderamente fuerte"_ que nos ayude a _"arrancar a la sociedad del actual status quo gobernado por la publicidad, el consumismo y el poder al mejor postor"_.

Así que...

**Les damos la bienvenida al Fediverso y a FEDisroot**.

---
