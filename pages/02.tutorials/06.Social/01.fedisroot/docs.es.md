---
title: FEDisroot
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - pleroma
        - fedisroot
page-toc:
    active: false
---

![](en/fedisroot.png)

# FEDisroot: la instancia de Akkoma de Disroot

**Akkoma** es un servicio federado de microblogging. Federado significa que puede comunicarse con otros servicios y servidores que soporten los mismos estándares abiertos como, por ejemplo, **Mastodon**, **Pleroma**, **GNU Social**, **Hubzilla**, **Misskey**, **PeerTube** o **Friendica**.

Así que no importa cuál de esos servicios estén usando, pueden interactuar con cualquier persona del Fediverso.

Akkoma de **Disroot** es bastante intuitivo de utilizar. En los siguientes capítulos aprenderemos sobre cada aspecto de su funcionamiento.

---

# Contenidos
1. [Interfaz](interface)

2. [Configuraciones](settings)

3. [Interacción](interacting)
