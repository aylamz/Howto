---
title: FEDisroot
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - pleroma
        - fedisroot
page-toc:
    active: false
---

![](en/fedisroot.png)

# FEDisroot : l'instance Akkoma de Disroot

**Akkoma** est un service fédéré de microblogging. Fédéré signifie qu'il peut communiquer avec d'autres services et serveurs qui supportent les mêmes standards ouverts comme, par exemple, **Mastodon**, **Pleroma**, **GNU Social**, **Hubzilla**, **Misskey**, **PeerTube** ou **Friendica**.

Ainsi, peu importe lequel de ces services vous utilisez, vous pouvez interagir avec n'importe qui dans le Fediverse.

L'utilisation d'Akkoma de Disroot est assez intuitive. Dans les chapitres suivants, nous allons découvrir tous les aspects de son fonctionnement.

---

# Table des matières
1. [Interface](interface)

2. [Paramètres](settings)

3. [Interaction](interacting)
