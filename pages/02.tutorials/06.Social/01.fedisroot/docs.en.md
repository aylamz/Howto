---
title: FEDisroot
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - pleroma
        - fedisroot
page-toc:
    active: false
---

![](en/fedisroot.png)

# FEDisroot: Disroot's Akkoma instance

**Akkoma** is a microblogging federated service. Federated means that it can communicate with other services and servers that support the same open standards like, for example, **Mastodon**, **Pleroma**, **GNU Social**, **Hubzilla**, **Misskey**, **PeerTube** or **Friendica**.

So not matter which of those services you are using, you can interact with anyone in the Fediverse.

Disroot's Akkoma is pretty intuitive to use. In the following chapters we will learn about every aspect of how it works.

---

# Table of Content
1. [Interface](interface)

2. [Settings](settings)

3. [Interacting](interacting)
