---
title: 'Pont Matrix'
published: true
visible: true
indexed: false
updated:
        last_modified: Mars, 2021
        app: Bifrost
        app_version: 0.2.0
taxonomy:
    category:
        - docs
    tags:
        - chat
        - xmpp
        - matrix
        - pont
page-toc:
    active: false
---

# Pont Matrix
Vous pouvez vous connecter à n'importe quelle salle **Matrix** hébergée sur n'importe quel serveur **Matrix** via le **Pont Matrix Bifrost** (_**qui est hébergé par Matrix.org**_). Pour rejoindre une salle Matrix :

`#salle#matrix.domaine.tld@matrix.org`

Où `#salle` est la salle **Matrix** que vous voulez rejoindre et `matrix.domaine.tld` est l'adresse du **serveur Matrix** que vous voulez rejoindre. Assurez-vous de laisser `@matrix.org` tel quel, car c'est l'adresse du **pont Matrix**.

!! ### REMARQUE IMPORTANTE
!! Comme il est mentionné dans la page de code de [**Bifrost**](https://github.com/matrix-org/matrix-bifrost):<br>
!! ###### `Ce pont est en développement très actif actuellement et destiné principalement à des fins d'expérimentation et d'évaluation`.
!! **Donc, veuillez garder à l'esprit que ce "pont" est instable et qu'il peut parfois être hors service**.
