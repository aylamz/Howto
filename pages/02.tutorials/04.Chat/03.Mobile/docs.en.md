---
title: Chat: Mobile clients
updated:
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - chat
        - xmpp
page-toc:
    active: false
---

# Chat Clients for Mobile

### Android
- [Conversations](android/conversations)

### SailfishOS
- [Chat app](sailfishos)

### iOS
- [Chat Secure](ios)
