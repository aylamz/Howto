---
title: Circles
published: true
visible: false
indexed: true
updated:
        last_modified: " 2021"
        app: Circles
        app_version: 0.20.6
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - circles
visible: true
page-toc:
    active: false
---

# Circles (coming soon)

**Circles** allows you to create your own groups of users/colleagues/friends. Those groups (or 'circles') can then be used by any other app for sharing purpose (files, social feed, status update, messaging, etc).
