---
title: "Apps"
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

# Cloud Apps

One of the most powerful features of **Nextcloud** is the ability to expand its functionality through **applications**. These are essentially plugins or extensions that allow to add extra functions to the **Cloud** environment.

Below you will find guides and tutorials about of those that are currently available in the **Disroot Cloud** to learn how to use or configure them on different devices.

---
