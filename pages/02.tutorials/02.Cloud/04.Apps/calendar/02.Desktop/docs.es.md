---
title: "Calendario: Escritorio"
published: true
visible: false
updated:
        last_modified: "Julio 2019"
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

# Integración con el escritorio del Calendario

Puedes leer los siguientes tutoriales para tener tus **Calendarios** sincronizados utilizando clientes de escritorio multiplataforma.

- [Thunderbird: Calendario / Contactos / Sincronización de Tareas](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts)
- [calcurse: Sincronización de calendarios](/tutorials/cloud/clients/desktop/multiplatform/calcurse-caldav)

Alternativamente, puedes configurar y utilizar la integración con el escritorio.

- [GNOME: Integración con el escritorio](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
- [KDE: Integración con el escritorio](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
