---
title: Calendar
published: true
visible: false
indexed: true
updated:
        last_modified: "February 2022"
        app: Calendar
        app_version: 2.1.3
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - app
        - calendar
page-toc:
    active: false
---

# Calendario

L'app **Calendar** è l'interfaccia utente per il server CalDAV di **Disroot**. Ti permette di sincronizzare gli eventi da vari dispositivi con il tuo **Disroot Cloud** e di modificarli online.

---

## [Interfaccia web](web)
- Crea e configura i calendari

## [Client desktop](desktop)
- Client desktop e impostazioni di integrazione per organizzare e sincronizzare i calendari

## [Client mobile](/tutorials/cloud/clients/mobile)
-  Client mobile e impostazioni per organizzare e sincronizzare i calendari
