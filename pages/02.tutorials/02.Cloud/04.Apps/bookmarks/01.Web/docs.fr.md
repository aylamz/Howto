---
title: Signets: Web
published: true
visible: false
updated:
        last_modified: "Juillet 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - signets
page-toc:
    active: false
---

## Signets
