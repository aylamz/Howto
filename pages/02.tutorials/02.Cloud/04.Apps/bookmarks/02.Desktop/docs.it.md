---
title: Bookmarks: Desktop
published: true
visible: false
updated:
        last_modified: "Febbraio 2022"
        app:
        app_version:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - bookmarks
page-toc:
    active: false
---

## Segnalibri
