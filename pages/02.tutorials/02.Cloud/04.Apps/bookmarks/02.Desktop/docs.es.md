---
title: Marcadores: Escritorio
published: true
visible: false
updated:
        last_modified: "Julio 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - nube
        - marcadores
        - escritorio
page-toc:
    active: false
---

## Marcadores
