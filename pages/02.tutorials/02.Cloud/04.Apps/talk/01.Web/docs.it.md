---
title: Talk: Web
published: true
visible: false
updated:
        last_modified: "February 2022"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - talk
        - call
page-toc:
     active: false
---

# Nextcloud Talk

Da gennaio 2017 abbiamo introdotto le chiamate rapide nel nostro cloud. È un'app per audio/videoconferenza molto semplice che puoi utilizzare per comunicare non solo con altri utenti disroot ma con chiunque abbia un computer connesso a Internet e un browser Web aggiornato che supporta la tecnologia WebRTC. 

![](en/spreed_main.png)

L'interfaccia dell'applicazione è molto semplice. La barra laterale sinistra mostra l'elenco delle stanze che hai creato.


In basso a destra della finestra vedrai il tuo avatar e alcune opzioni in cui puoi: 

![](en/spreed_bottom.png)

- disattivare/riattivare il microfono
- accendere/spegnere la fotocamera
- attivare/disattivare la modalità a schermo intero

Questo è praticamente tutto. Dovresti essere pronto per iniziare la tua nuova chiamata

# Chiamare 

Per iniziare a chiamare, devi creare una stanza. Clicca su **"Scegli persona"** per farlo. Nella finestra popup hai un'opzione per invitare un account disroot esistente o creare una stanza pubblica.
Vedrai che c'è una nuova stanza elencata nella barra laterale di sinistra.
Quando si fa clic sul pulsante delle opzioni della stanza, è possibile:
- invitare più persone
- creare/eliminare collegamento pubblico
- lasciare la chiamata/sala 
   
![](en/spreed_create_calls1.png)

Non vi è alcuna reale differenza tra le stanze utente disroot e le stanze pubbliche poiché in entrambi i casi è possibile invitare utenti disroot o creare/rimuovere stanze di collegamento pubbliche.

Una volta avviata la chiamata premendo sul nome della stanza, verrà inviata una notifica all'utente disroot invitato o (in caso di istanza di una stanza pubblica) si dovrà attendere che almeno un altro partecipante si unisca.

Una volta che si uniscono, puoi iniziare a fare la tua videochiamata. 
