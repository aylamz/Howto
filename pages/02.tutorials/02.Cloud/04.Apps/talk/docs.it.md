---
title: Talk
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - talk
visible: true
page-toc:
    active: false
---

# Talk

Chat, video & conferenze audio con WebRTC

---

## [Interfaccia web](web)
