---
title: Talk
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - talk
visible: true
page-toc:
    active: false
---

# Talk

Chat, video & audio-conferencing using WebRTC

---

## [Web interface](web)
