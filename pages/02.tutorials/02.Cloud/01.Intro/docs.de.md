---
title: 'Cloud: Benutzeroberfläche'
published: true
visible: true
indexed: true
updated:
        last_modified: "June 2020"
        app: Nextcloud
        app_version: 18
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - nextcloud
        - interface
page-toc:
  active: true
---

# Die Benutzeroberfläche (UI)

<br>

![](de/main.png)

Dies ist die Hauptansicht. Nach dem Anmelden wird Deine **Cloud** wahrscheinlich in etwa so aussehen. Also fangen wir auch hier an, seine Bereiche und Funktionen kennzulernen.

## 1. App-Navigationsleiste
In dieser Leiste findest Du alle verfügbaren oder integrierten Anwendungen der **Disroot**-Cloud, wie zum Beispiel **Email**, **Dateien** und **Task**manager, **Notizen**, **Kalender**, **Kontakte**, **Lesezeichen**, **Photos**, **Talk**, **Kreise**, **Deck** und der **Aktivitäten**bericht.<br> *Um mehr über die Anwendungen zu erfahren, sieh Dir [dieses Kapitel](/tutorials/cloud/apps) an.*

*Wir haben nach sorgfältiger Abwägung entschieden, den RSS-Feeder **News** zu deaktivieren*

  ![](de/app_bar.gif)

## 2. App-Informationsbereich
Wenn Du mit einer App arbeitest, werden hier zugehörige Informationen, Optionen und Filter erscheinen.

  ![](de/app_info.gif)

## 3. Haupt-Appansicht
Dieser Bereich zeigt die Inhalte der gewählten App.

  ![](de/app_view.png)

## 4. Home
Dieser Button ![](de/nav_button.png) bringt Dich zurück zu Deinem Root-Verzeichnis (das Hauptverzeichnis), wenn Du durch Deine Dateiverzeichnisse navigierst.

  ![](de/nav_button.gif)

## 5. Neu
Dies ermöglicht es Dir, eine Datei hochzuladen und neue Verzeichnisse, Textdateien oder Pads zu erstellen.

  ![](de/new_button.gif)

## 6. Umfangreicher Arbeitsbereich
Dies ermöglicht es Dir, Deine Verzeichnisse in einen Kontext zu setzen, Notizen, Dateiverweise und sogar Todo-Listen hinzuzufügen. Das ist besonders nützlich, um Deine Arbeit mit Anderen zu koordinieren und sicherzustellen, dass jeder weiß, wofür ein bestimmtes Verzeichnis gedacht ist. Du kannst hier außerdem eine kurze Liste mit den zuletzt geänderten Dateien und Verzeichnissen sehen.

  ![](de/workspace.gif)

Du kannst den umfangreichen Arbeitsbereich ein- und ausschalten, indem Du auf das Einstellungen-Icon im unteren Bereich der linken Leiste klickst.

  ![](de/workspace_set.gif)

## 7. Suche
Durch einen Klick auf das Lupen-Icon kannst Du nach Dateien suchen.

  ![](de/search.gif)

## 8. Benachrichtigungen
Jedes Mal, wenn Du einen Link, ein Verzeichnis oder eine Datei mit jemandem teilst oder jemand mit Dir, ein Dokument oder eine Datei geändert wird oder ein Administrator oder anderer Nutzer Dir eine Nachricht sendet, wirst Du hier benachrichtigt (ein hervorgehobener Punkt erscheint).

  ![](de/notification.png)

## 9. Kontakte
Hier findest Du Deine Kontakte und die Nutzer auf dem Server.

  ![](de/contacts.png)

## 10. Einstellungsmenü
Wenn Du darauf klickst, erscheint ein Dropdownmenü und Du kannst auf die persönlichen und Anwendungs-Einstellungen zugreifen. Einige dieser Einstellungen ermöglichen es Dir, Dein Profil, Passwort, Sprache, Benachrichtigungseinstellungen, verbundene Geräte u.s.w. zu verwalten und zu ändern. Im nächsten Kapitel werden wir uns das im Detail ansehen.

  ![](de/settings.gif)

## 11. Ansicht wechseln
Indem Du hier klickst, kannst Du ändern, wie Deine Dateien in der Hauptansicht angezeigt werden. Du kannst zwischen Rasteransicht und Iconansicht wechseln.

  ![](de/view.gif)
