---
title: Clientes para el escritorio: GNU/Linux
taxonomy:
    category:
        - docs
visible: false
page-toc:
     active: false
---
|![](gnu_linux.png)|
|:--:|
|Nextcloud se integra con GNU/Linux enormemente. Debajo puedes encontrar links que te ayudrán a tener todo en funcionamiento.|

 ## Contenidos
 - [Sincronizando archivos con el cliente de escritorio](desktop-sync-client)
 - [GNOME: Integración con el escritorio](gnome-desktop-integration)
 - [KDE: Integración con el escritorio](kde-desktop-integration)
 - [Aplicación de Noticias: Sincronizando con tu escritorio](news-app-syncing)
