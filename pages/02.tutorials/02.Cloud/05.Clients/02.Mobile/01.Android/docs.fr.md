---
title: Android
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - android
        - nextcloud
visible: true
page-toc:
     active: false
---

# Android : Clients et intégration de Nextcloud

## [Application Nextcloud](nextcloud-app)
Configuration et utilisation de l'application Nextcloud avec un compte Disroot.

## [DAVx⁵](calendars-contacts-and-tasks)
Comment synchroniser les calendriers, les contacts et les tâches.

## [Migration des contacts dans Android](migrating-contacts-from-google)
Comment migrer les contacts de Google vers Nextcloud ?
