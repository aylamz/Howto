---
title: 'Mumble'
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - Mumble
        - Plumble
        - audioconference
        - audio
page-toc:
  active: false
---

![](/home/icons/mumble.png)

**Mumble** è un software di chat vocale open source, a bassa latenza e di alta qualità destinato principalmente all'uso durante il gioco, ma che puoi utilizzare per qualsiasi audioconferenza/chat.
<br>Non è necessario alcun account per usarlo.

## Client
Puoi utilizzare Mumble da pc (desktop) o dallo smartphone.

### [Desktop: Mumble](mumble)
- Accedi ai canali dal tuo pc.

### [Android: Plumble](plumble)
- Accedi ai canali dal tuo dispositivo Android.

### [iOS: Mumble](mumbleios)
- Accedi ai canali dal tuo dispositivo iOS.
