---
title: 'Utilisateurs enregistrés'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - profil
        - configurations
        - cryptpad
page-toc:
    active: true
---

# Utilisateurs enregistrés
Les utilisateurs enregistrés bénéficient de fonctionnalités supplémentaires :

- **L'espace de stockage** des documents est personnel, permanent et peut accueillir des fichiers multimédia.
- **Options de gestion des fichiers** :
  * ajouter un mot de passe, une date d'expiration ou une liste d'accès.
  * Organiser les documents en dossiers, dossiers partagés, ou avec des tags et des modèles.
- Créer et travailler avec des **Équipes**.
- Personnalisation du **Profil**.
- Fonctionnalités **Contacts** (pour partager des documents et chatter avec eux)
- **Notifications** pour les interactions entre les contacts.

---
Dans les prochains chapitres, nous apprendrons comment configurer CryptPad, ses bases et comment gérer CryptDrive.

- ## [Configurations](configurations)
- ## [CryptDrive](cryptdrive)
- ## [Partage & Accès](sharing)
