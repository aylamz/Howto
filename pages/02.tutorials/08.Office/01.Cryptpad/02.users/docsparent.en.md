---
title: 'Users'
updated:
published: true
visible: true
indexed: false
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - office
        - collaboration
page-toc:
  active: false
---

# Users
Both type of users, **guests** (non-registered) and **registered**, have access to all the **CryptPad** suite applications. However, options, settings and functionalities are different for one and the other.

## [Guest user](guest)
* [Settings and configurations](guest/configurations)
* [CryptPad basics](guest/basics)
* [CryptDrive](guest/cryptdrive)


## [Registered user](registered)
* [Settings and configurations](registered/configurations)
* [CryptDrive](registered/cryptdrive)
* [Share & Access](registered/sharing)
