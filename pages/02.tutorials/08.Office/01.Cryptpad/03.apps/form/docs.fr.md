---
title: Formulaire
published: true
visible: false
indexed: true
updated:
        last_modified: "Septembre 2021"
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - formulaire
        - applications
visible: true
page-toc:
    active: false
---

# Formulaire

Créer et gérer des enquêtes/sondages
