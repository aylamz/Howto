---
title: Présentation
published: true
visible: false
indexed: true
updated:
        last_modified: "Septembre 2021"
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - présentation
        - applications
visible: true
page-toc:
    active: false
---

# Présentation

Créez des présentations et des diapositives.
