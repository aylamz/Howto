---
title: 'Pads'
updated:
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - Pad
        - Etherpad
page-toc:
  active: false
---

![](/home/icons/etherpad.png)
**Etherpad** es una aplicación colaborativa multi-usuarixs de edición en tiempo real de documentos de texto. Puedes acceder a ella desde: [https://pad.disroot.org](https://pad.disroot.org).<br>No es necesario una cuenta para utilzarla. Sin embargo, nuestra nube tiene un complemento muy útil que te ayuda a mantener un registro de todos tus pads, como si se trataran de uno de tus archivos.

!! **Los "pads" no son archivos conteniendo tus datos sino vínculos a tu trabajo alojado en https://pad.disroot.org**

# La idea detrás de los pads...
... es muy simple. Es un editor de texto que "vive" en la web. Todo lo que escribes en ellos se guarda automáticamente. Puedes trabajar en un documento con varias personas al mismo tiempo sin necesidad de guardar y pasarse copias del documento unxs a otrxs. Una vez que el trabajo está realizado, puedes exportar el pad a un formato de tu elección.

### [Etherpad](etherpad)
- Editor de texto web colaborativo

### [Padland](padland)
- Gestor de pads para Android
