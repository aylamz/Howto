---
title: Uživatel
subtitle: Správa účtu a osobních dat
icon: fa-user
published: true
visible: true
taxonomy:
    category:
        - docs
        - topic
page-toc:
    active: false
---

# Uživatel
V této části najdete užitečné informace o správě svého účtu.
<br>
