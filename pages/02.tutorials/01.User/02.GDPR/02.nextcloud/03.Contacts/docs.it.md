---
title: "Nextcloud: Exporting Contacts"
published: true
indexed: true
updated:
    last_modified: "February 2022"		
    app: Nextcloud
    app_version: 18
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
visible: true
page-toc:
    active: false
---

I contatti possono essere esportati in qualsiasi momento in modo molto semplice.

- Accedi al [cloud](https://cloud.disroot.org)
- Seleziona l'applicazione "*Contatti*".

![](en/select_app.gif)

- Premi il pulsante di download accanto alla rubrica che desideri esportare. I contatti vengono salvati in formato .vcf.

![](en/export.gif)
