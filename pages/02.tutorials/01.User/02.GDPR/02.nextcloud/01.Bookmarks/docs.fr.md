---
title: 'Nextcloud: Exporter les Favoris'
published: true
indexed: true
updated:
    last_modified: "Juin 2020"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - cloud
        - favoris
        - rgpd
visible: true
page-toc:
    active: false
---

L'exportation de vos données de signets stockées sur le nuage **Disroot** est très simple.

  - Connectez-vous au [nuage] (https://cloud.disroot.org).
  - Sélectionnez l'application **Signet**.

  ![](en/select.gif)

  - Sélectionnez **Paramètres** (en bas de la barre latérale gauche) et appuyez sur le bouton **Export**.

  ![](en/export.gif)

  - Sélectionnez l'endroit où enregistrer le fichier.
