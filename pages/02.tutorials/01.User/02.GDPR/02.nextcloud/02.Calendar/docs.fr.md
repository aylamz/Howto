---
title: Nextcloud: Exporter ses agendas
published: true
indexed: true
updated:
    last_modified: "Juin 2020"		
    app: Nextcloud
    app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - cloud
visible: true
page-toc:
    active: false
---

L'exportation des calendriers est assez simple :

  - Connectez-vous au [cloud](https://cloud.disroot.org)
  - Sélectionnez l'application Calendrier

  ![](en/select.gif)

  - Pour exporter un de vos calendriers ou les calendriers auxquels vous êtes abonné, sélectionnez l'option de menu *"trois points "* à côté du calendrier et cliquez sur l'option *"Exporter "*. Le calendrier exporté est enregistré au format .ics.

  ![](en/export.gif)

  - Répétez le processus pour tous les autres calendriers que vous souhaitez exporter.
