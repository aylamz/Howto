---
title: 'Export data: Nextcloud Files & Notes'
published: true
indexed: true
updated:
    last_modified: "February 2022"		
    app: Nextcloud
    app_version: 18
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - files
        - notes
        - gdpr
visible: true
page-toc:
    active: false
---

Puoi scaricare i tuoi file i n modo molto semplice su **Nextcloud**:app.

- Accedi al tuo [cloud](https://cloud.disroot.org)
- Seleziona l'app **File**
- Seleziona tutti i file facendo clic sulla casella di controllo
- Quindi fai clic sul menu **Azioni** e seleziona *Scarica* 

![](en/files_app.gif)
