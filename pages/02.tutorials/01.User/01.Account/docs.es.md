---
title: 'Cuenta'
updated:
published: true
visible: true
indexed:
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - cuenta
        - administración
page-toc:
  active: false
---

# Gestión de la cuenta

## [Administración](administration)
Modificar o restablecer contraseñas, notificaciones, preguntas de seguridad, acceso a formularios y borrar la cuenta.

## [Solicitud de Alias](alias-request)
Cómo solicitar alias de correo.
