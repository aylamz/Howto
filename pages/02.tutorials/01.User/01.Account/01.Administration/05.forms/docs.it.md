---
title: 'Requests Forms'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - forms
page-toc:
    active:
---

# Moduli per richieste personalizzate

![](en/dashboard_forms.png)

Da qui puoi accedere ai moduli per richiedere:
- Alias di posta elettronica aggiuntivo
- Collega il tuo dominio personale
- Richiedi spazio di archiviazione aggiuntivo per la tua casella di posta
- Richiedi spazio di archiviazione aggiuntivo per il tuo **Cloud**. 

Basta fare clic sull'opzione che ti serve e si aprirà il modulo da completare. 

![](en/forms.png)
