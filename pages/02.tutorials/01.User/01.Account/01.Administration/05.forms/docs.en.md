---
title: 'Requests Forms'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - forms
page-toc:
    active:
---

# Custom Requests forms

![](en/dashboard_forms.png)

From here you can access the forms to request:
- Additional email alias
- Link your personal domain
- Request extra storage for your mailbox, or...
- Request extra storage for your **Cloud**.

Just click on the option you need and the form will open for you to complete.

![](en/forms.png)
