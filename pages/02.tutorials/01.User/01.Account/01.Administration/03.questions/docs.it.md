---
title: 'Security Questions'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - management
page-toc:
    active:
---

# Imposta le tue domande di sicurezza 

![](en/dashboard_questions.png)

Nel caso in cui dimentichi/perdi la password, puoi reimpostarla senza l'intervento dell'amministratore impostando prima le domande di sicurezza. Per farlo, clicca su questa opzione.

Il processo è piuttosto semplice. 

- Clicca su ***Imposta le domande di sicurezza***.

 ![](en/sec_qs_01.png)

- Scrivi la prima domanda e la relativa risposta, quindi seleziona le due domande successive dall'elenco a discesa e scrivi anche le risposte.

  ![](en/sec_qs_02.png)

- Una volta che le risposte soddisfano i requisiti, fai clic su **Salva risposte** e infine su **Continua** 

  ![](en/sec_qs_03.png)
