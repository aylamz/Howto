---
title: 'Profil du compte'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - compte
        - profil
page-toc:
    active:
---

# Mise à jour de votre profil
Que ce soit pour recevoir des notifications ou réinitialiser votre mot de passe, vous pouvez ajouter une adresse électronique secondaire à votre profil. Vous pouvez également modifier votre pseudonyme (le nom qui apparaîtra sur les écrans des services).

![](en/dashboard_profile.png)

Complétez/mettez à jour les informations.

![](en/profile_update.png)

- **Nom d'écran** : c'est le nom que vous choisissez pour vous identifier dans les différents services qui nécessitent des informations d'identification **Disroot**.
- **Email de notification** : l'adresse e-mail où vous recevrez les informations importantes relatives à votre compte. De temps en temps, vous pouvez également recevoir des informations nous concernant, telles que des annonces de services ou d'améliorations ou le rapport annuel de **Disroot**.
- **Email de réinitialisation du mot de passe** : vous pouvez ajouter/modifier une adresse e-mail secondaire afin de pouvoir l'utiliser pour réinitialiser votre mot de passe au cas où vous le perdriez/oublieriez.
