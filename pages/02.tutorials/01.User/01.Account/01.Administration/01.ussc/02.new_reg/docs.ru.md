---
title: 'Регистрация новой учётной записи'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - registration
page-toc:
    active: false
---

# Регистрация новой учетной записи
Хотя процесс довольно прост, мы рассмотрим его шаг за шагом.

!! **При регистрации учётной записи обратите внимание на следующее:**
!! - Используйте то же устройство, с которого вы выполняли запрос, что бы открыть ссылку для подтверждения (иначе регистрация может завершиться ошибкой).
!! - Через 15 минут сессия истекает и нужно будет начинать процесс заново.
!! - Если вы используете TOR или VPN и ваш IP-адрес изменился во время процесса, это будет рассматриваться как атака на систему безопасности, и поэтому ваша сессия будет заблокирована.


## Регистрация шаг за шагом
### 01: Имя пользователя
Это будет имя вашей учетной записи и адрес электронной почты, которые нельзя изменить после создания. Имя вашей учетной записи будет выглядеть так: _**username@disroot.org**_

![](en/reg_01.png)


### 02: Отображаемое имя
Это имя будет отображаться для служб, требующих входа в систему, таких как **облако** или **электронная почта**. Обычно это то же имя, что и ваше имя пользователя, но вы можете выбрать что угодно.<br>
Например, ваше имя пользователя может быть _username_, а отображаемое имя — _User Name_.

![](en/reg_02.png)


### 03: Вопрос
Проверочный вопрос предназначен для проверки того, что бы убедиться, что вы человек, а не спам бот. А также, для того, что бы держать спамеров как можно дальше.

!! **Ответ должен соответствовать следующим требованиям**:
!! - Длинна должна быть не менее 150 символов.
!! - Это должен быть ответ на вопрос, не более того.
!! - Это не может быть цитатой или копией/вставкой из внешнего источника.
!! - Это может быть на любом языке, хотя мы предлагаем вам использовать английский.
!! - Он не может содержать определенные или специальные символы из другого языка, кроме английского. Это означает, что вам следует избегать использования ударений или, например, буквы ñ.

![](en/reg_03.png)


### 04: Электронное письмо с подтверждением
Для завершения процесса регистрации необходимо отправить вам код подтверждения на действующий адрес электронной почты. Также на этом этапе вы можете использовать этот же адрес на случай, если вам понадобится сбросить пароль в будущем. Если вы предпочитаете этого не делать, **не забудьте сохранить свой пароль**, так как **этот адрес электронной почты удаляется из нашей базы данных после одобрения/отклонения запроса, и мы не можем сбросить его**.

![](en/reg_04.png)

Затем дважды введите свой пароль, убедившись, что он соответствует условиям для его создания, и нажмите кнопку **Продолжить**, чтобы закончить.

И это всё! **\0/**

!! #### ВНИМАНИЕ<br>
!! **Запросы на учетные записи проверяются вручную каждый день, один за другим, поэтому рассмотрение может занять до 48 часов.**