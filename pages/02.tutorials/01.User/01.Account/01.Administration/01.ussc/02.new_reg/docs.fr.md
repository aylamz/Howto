---
title: 'Enregistrement d'un nouveau compte'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - compte
        - enregistrement
page-toc:
    active: false
---

# Enregistrement d'un nouveau compte
Bien que le processus soit assez simple, nous allons le voir étape par étape.

!! **Lorsque vous enregistrez un compte, veuillez garder les points suivants à l'esprit :**
!! - Utilisez le même appareil que celui avec lequel vous remplissez le formulaire pour ouvrir le lien de vérification (sinon, l'enregistrement pourrait échouer).
!! - Après 15 minutes, la session expire et vous devrez recommencer le processus.
!! - Si vous utilisez TOR ou un VPN et que votre IP a changé pendant le processus, cela sera considéré comme une attaque de sécurité et votre session pourrait être bloquée.


## Enregistrement étape par étape
### 01: Le nom d'utilisateur
Il s'agira du nom de votre compte et de votre e-mail et ne pourra pas être modifié après sa création. Le nom de votre compte ressemblera à _**nomdutilisateur@disroot.org**_.

![](en/reg_01.png)


### 02: Le nom d'écran
Le nom d'écran est le nom qui sera affiché dans les services qui nécessitent une connexion, comme **Cloud** ou **Email**. Habituellement, c'est le même que votre nom d'utilisateur, mais vous pouvez choisir ce que vous voulez.<br>
Par exemple, votre nom d'utilisateur pourrait être _nom d'utilisateur_ et votre nom d'écran _Nom d'Utilisateur_.

![](en/reg_02.png)


### 03 : La question
La question de vérification a pour but de vérifier que vous êtes humain, et non un robot spammeur, et d'éloigner le plus possible les spammeurs.

! ! **La réponse doit remplir les conditions suivantes** :
! ! - Elle doit comporter au moins 150 caractères.
! ! - Elle doit être la réponse à la question, rien de plus.
! ! - Il ne peut s'agir d'une citation ou d'un copier/coller d'une source externe.
! ! - Elle peut être rédigée dans n'importe quelle langue, mais nous vous suggérons d'utiliser l'anglais.
! ! - Il ne peut pas contenir de caractères spécifiques ou spéciaux d'une autre langue que l'anglais. Cela signifie que vous devez éviter d'utiliser des accents ou, par exemple, la lettre ñ.

![](en/reg_03.png)


### 04 : L'e-mail de vérification
Afin de compléter le processus d'enregistrement, il est nécessaire de vous envoyer un code de vérification à une adresse électronique valide. À cette étape également, vous pouvez choisir d'utiliser cette même adresse pour une future réinitialisation de votre mot de passe, au cas où vous en auriez besoin. Si vous préférez ne pas le faire, **n'oubliez pas de garder votre mot de passe en sécurité** car **cette adresse électronique est supprimée de notre base de données une fois la demande acceptée/refusée et nous ne pouvons pas la réinitialiser**.

![](en/reg_04.png)

Saisissez ensuite deux fois votre mot de passe, en vérifiant qu'il respecte les règles en la matière, et cliquez sur le bouton **Continue** pour terminer.

Et voilà, c'est fait ! **\0/**

!! #### NOTE<br>
!! **Les demandes de compte sont examinées manuellement tous les jours et l'approbation peut donc prendre jusqu'à 48 heures.**
