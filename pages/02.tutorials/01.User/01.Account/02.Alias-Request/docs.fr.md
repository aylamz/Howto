---
title: 'Comment demander un alias de courriel'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - alias
page-toc:
    active: false
---

# Demander un Alias d'Email

Des alias sont disponibles pour les sympathisants réguliers. Par sympathisants réguliers, nous entendons ceux qui nous "payent" au moins une tasse de café par mois.

Ce n'est pas que nous faisons la promotion du café, le café est en fait un symbole très pratique pour [l'exploitation et l'inégalité](http://www.foodispower.org/coffee/). Et nous pensons que c'était une bonne façon de permettre aux gens de mesurer eux-mêmes ce qu'ils peuvent donner.

Veuillez prendre le temps de réfléchir à votre contribution. 

Si vous pouvez nous "acheter" une tasse de café **Rio De Janeiro** par mois, c'est bien, mais si vous avez les moyens d'acheter un double décaféiné Frappuccino au soja avec un extra de crème par mois, alors vous pouvez vraiment nous aider à maintenir la plateforme **Disroot** en fonctionnement et vous assurer qu'elle est disponible gratuitement pour d'autres personnes avec moins de moyens.

Nous avons trouvé cette [liste](https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee) des prix des tasses de café à travers le monde, elle n'est peut-être pas très précise, mais elle donne une bonne indication des différents tarifs.

Pour demander des alias, vous devez remplir ce [formulaire](https://disroot.org/fr/forms/alias-request-form).

Si vous cherchez le tutoriel sur comment configurer un alias email, jetez un oeil [ici](/tutorials/email/alias)
