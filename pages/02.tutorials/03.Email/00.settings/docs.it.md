---
title: Clients Settings
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - settings
page-toc:
    active: false
---

# Impostazioni del server
La maggior parte dei client di posta elettronica sarà in grado di rilevare automaticamente l'impostazione corretta del server, ma in alcuni casi potrebbe essere necessario inserire manualmente le seguenti informazioni per configurare il client di posta elettronica.

**IMAP Server**: disroot.org <br>
**SSL Port**: 993 <br>
**Autenticazione**: Password normale

**SMTP Server**: disroot.org <br>
**STARTTLS Port**: 587 <br>
**Autenticazione**: Password normale

**SMTPS Server**: disroot.org <br>
**TLS Port**: 465 <br>
**Autenticazione**: Password normale

**POP Server**: disroot.org <br>
**SSL Port**: 995 <br>
**Autenticazione**: Password normale

---

#### Guide correlate:
- [**Webmail**](/tutorials/email/webmail)
- [**Client Desktop**](/tutorials/email/clients/desktop)
- [**Client dispositivi mobili**](/tutorials/email/clients/mobile)
