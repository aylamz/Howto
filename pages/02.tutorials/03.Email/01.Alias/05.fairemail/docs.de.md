---
title: Email Alias: Einrichtung in FairEmail
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - fairemail
        - android
page-toc:
    active: true
---

# Alias in FairEmail

Als erstes öffnest Du **FairEmail** und öffnest in den Haupteinstellungen Punkt 2, "Identitäten einrichten", indem Du auf den *Verwalten*-Button tippst (3 Punkte oben links '>' Einstellungen '>' Reiter "Haupteinstellungen").

![](de/fairemail_alias_01.png)

In den Einstellungen drückst Du lange auf Dein **Disroot**-Emailkonto, bis sich ein Menü öffnet. Dort gehst Du auf *Kopiere...*.

![](de/fairemail_alias_02.png)

Du gelangst nun in die Eingabemaske für eine Identität, die mit Deinem **Disroot**-Konto verknüpft ist. Gebe Deine Daten ein bzw. ändere die automatisch befüllten Felder entsprechend Deiner Wünsche ab.

*(Jeder* **Disroot**-*Nutzer hat automatisch ein* benutzername@disr.it *Alias zur Verfügung)*

![](de/fairemail_alias_03.png)

Beende Deine Engabe, indem Du unter der Eingabemaske auf den *Speichern*-Button tippst. Dein Alias ist nun angelegt.

![](de/fairemail_alias_04.png) ![](de/fairemail_alias_05.png)

# Email verfassen
Um eine Email mit Deinem neuen Alias zu versenden, tippe bei der Email-Erstellung auf das *Von:*-Eingabefeld und wähle das von Dir gewünschte Alias aus dem Dropdown-Menü aus.

![](de/fairemail_alias_06.png)
