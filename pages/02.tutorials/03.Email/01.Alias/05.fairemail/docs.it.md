---
title: Email Alias: Setup on FairEmail
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - fairemail
        - android
page-toc:
    active: true
---

# Alias su FairEmail

Per prima cosa, apri **FairEmail** e vai su "Crea Identità" nelle impostazioni principali, punto 2, toccando il pulsante "Gestisci" (3 punti in alto a sinistra '>' Impostazioni '>' scheda "impostazioni principali ")

![](en/fairemail_alias_01.png)

Nelle impostazioni tocca e tieni premuto il tuo **Disroot**-Account finché non si apre un menu. In questo menu sceglierai *Copia...* 

![](en/fairemail_alias_02.png)

Nella seguente finestra di dialogo di input ti viene presentata un'identità, che è collegata al tuo account **Disroot** esistente. Inserisci i tuoi dati o modifica le informazioni compilate automaticamente come desideri.

*(Ogni* **Disroot** *l'utente ha un* nome utente@disr.it *alias da utilizzare per impostazione predefinita)* 

![](en/fairemail_alias_03.png)

Completa il tuo input toccando toccando il pulsante *Salva* nella parte inferiore del menu. Il tuo ALias è ora impostato

![](en/fairemail_alias_04.png) ![](en/fairemail_alias_05.png)

# Crea e-mail
Per inviare un'e-mail con il tuo nuovo alias, tocca il campo *Da:* e scegli l'alias che desideri utilizzare dal menu a discesa visualizzato 

![](en/fairemail_alias_06.png)
