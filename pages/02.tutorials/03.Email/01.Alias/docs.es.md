---
title: "Correo: Configurar Alias"
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - configuraciones
page-toc:
    active: false
---

# Alias de correo

Aquí debajo encontrarás cómo hacerlo en varios clientes de correo.

- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K9](k9)
- [Mail iOS](mailios)
