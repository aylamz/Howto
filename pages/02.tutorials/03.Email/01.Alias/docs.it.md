---
title: 'Alias setup'
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - settings
page-toc:
    active: false
---

# Alias di posta elettronica

Di seguito puoi trovare come gestire gli alias attraverso alcuni client di posta elettronica. 

- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K9](k9)
- [FairEmail](fairemail)
- [Mail iOS](mailios)
