---
title: Disroot Webmail
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: true
---

# Disroot Webmail

A webmail, or web-based email, is an email service that can be accessed using the web browser. This means you don't need to install an email client software on your device. The main advantage of webmail over the use of a desktop email client is the ability to send and receive email anywhere from a web browser.

**Disroot** webmail solution is powered by **Roundcube**.


![](roundcube/rc_logo.png)

# What is Roundcube?
Roundcube is a free and open source webmail software that provides all the functionalities you would expect from an email client: full support for MIME and HTML messages, multiple sender identities, address book with groups and LDAP connectors, threaded message listing, spell checking, support for access control lists (ACL), import/export functions and support for PGP encryption among many other features.

**[[Roundcube Site](https://roundcube.net/)] | [[Source code](https://github.com/roundcube/roundcubemail/)]**

Through the following tutorial, we will learn how to use **Roundcube** and get to know its features.

----

# Table of Contents

# [Migration Guide](migration)
  - [Migration FAQ](migration/faq)
  - [Backup/export and import your contacts](migration/backup)

# [Roundcube](roundcube)
## [01. Getting started](roundcube/getting_started)
  - Overview
  - Tasks and basic operations

## [02. Settings](roundcube/settings)
  - [01. Preferences](roundcube/settings/preferences)
    - User interface
    - Mailbox view
    - Displaying messages
    - Composing messages
    - Contacts
    - Special folders
    - Server settings
    - Deleting old messages
    - Message highlights
  - [02. Folders](roundcube/settings/folders)
  - [03. Identities](roundcube/settings/identities)
    - Default identity
    - Add other identities / aliases
    - Sending an email with another identity
  - [04. Responses](roundcube/settings/responses)
  - [05. Filters](roundcube/settings/filters)
  - [06. Account details](roundcube/settings/account_details)

## [03. Email](roundcube/email)
  - Composing an email

## [04. Contacts](roundcube/contacts)
  - Contacts lists
  - Importing contacts
  - Address book
  - Groups
