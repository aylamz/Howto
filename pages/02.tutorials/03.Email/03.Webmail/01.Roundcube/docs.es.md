---
title: "Cliente web de correo Roundcube"
published: true
visible: true
indexed: true
updated:
        last_modified: "Marzo, 2023"
        app: Roundcube Webmail
        app_version: 1.6.1
taxonomy:
    category:
        - docs
    tags:
        - correo
page-toc:
    active: true
---

![](rc_logo.png)

# Índice

## [01. Comenzando](getting_started)
  - Vista general
  - Tareas y operaciones básicas

## [02. Configuraciones](settings)
  - [01. Preferencias](settings/preferences)
    - Interfaz de usuarie
    - Vista de la bandeja de entrada
    - Visualización de mensajes
    - Redacción de mensajes
    - Contactos
    - Carpetas especiales
    - Configuraciones del servidor
    - Borrando mensajes viejos
    - Destacados de los mensajes
  - [02. Carpetas](settings/folders)
  - [03. Identidades](settings/identities)
    - Identidad por defecto
    - Agregar otras identidades / alias
    - Enviando un mensaje con otro identidad
  - [04. Respuestas](settings/responses)
  - [05. Filtros](settings/filters)
  - [06. Detalle de la cuenta](settings/account_details)

## [03. Correo](email)
  - Redactando un correo

## [04. Contactos](contacts)
  - Listas de contactos
  - Importando contactos
  - Libretas de direcciones
  - Grupos

## [05. Cifrado](encryption)