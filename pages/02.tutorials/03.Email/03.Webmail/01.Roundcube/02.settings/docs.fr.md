---
title: "Courriel Web: Paramètres"
published: true
visible: true
indexed: true
updated:
        last_modified: "Avril 2020"
        app: Roundcube Webmail
        app_version: 1.4.2
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Paramètres Courriel Web


# Table des matières
## [01. Préférences](01.preferences)
  - Interface utilisateur
  - Vue des boîtes aux lettres
  - Affichage des messages
  - Composer des messages
  - Contacts
  - Dossiers spéciaux
  - Paramètres du serveur
  - Suppression des anciens messages
  - Points forts des messages

## [02. Dossiers](02.folders)
## [03. identités](03.identities)
  - Identité par défaut
  - Ajouter d'autres identités / alias
  - Envoyer un e-mail avec une autre identité

## [04. réponses](04.responses)
## [05. Filtres](05.filters)
## [06. Détails du compte](06.account_details)