---
title: "Webmail : Paramètres / Détails du compte"
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - webmail
page-toc:
    active: true
---

# Paramètres

## Détails du compte

![Détails du compte](en/set_account.png)

Vous trouverez ici des informations sur :
  - Votre utilisateur e-mail
  - Votre système
  - Votre boîte aux lettres
  - La version de Roundcube

Vous trouverez également un bouton de don si vous souhaitez contribuer au **Projet Roundcube**.
