---
title: Mobiler Client: iOS Mail-Client
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Email in iOS

1. Öffne die Einstellungen Deines iOS-Geräts und gehe zu 'Mail, Kontakte, Calender'. Wähle dort 'Konto hinzufügen'.

![](en/ios_mail1.PNG)

2. Wähle 'Andere'.

![](en/ios_mail2.PNG)

3. Wähle 'Mail-Konto hinzufügen'.

![](en/ios_mail3.PNG)

4. Trage Deine Zugangsdaten ein und wähle 'Weiter'.

![](en/ios_mail4.PNG)

5. Ändere den Hostnamen zu **disroot.org**, sowohl für den eingehenden als auch für den ausgehenden Mailserver.

![](en/ios_mail5.PNG)

Wähle 'Weiter'. Dein Account sollte nun bereit sein, in Deinem iOS-Client genutzt zu werden.
