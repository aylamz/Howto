---
title: "Setting up your Disroot account"
visible: false
published: true
updated:
        last_modified: "April, 2021"
        app: Mozilla Thunderbird
        app_version: 78.9.0 on Manjaro Linux
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: true
---

# Setting up your Disroot account

The first time you run **Thunderbird**, you will be greeted by the account setup. Select **Skip this and use my existing email** to configure your Disroot account.<br>
If you are already using **Thunderbird** then you can select "**Email**" in the root view.

![](en/tb_setup_01.png)

## Fill in your account information

![](en/tb_setup_02.png)

 - **Your name:** *Name that will be displayed in the* "From" *field*
  ![](en/tb_setup_name.png)

 - **Email address:** *your_username @ disroot.org*
  ![](en/tb_setup_email.png)

 - **Password:** *your_super_secret_password*
 - **Remember Password:** If you want **Thunderbird** to remember your password and not prompt you for it every time you start the client, then check this option.

  ![](en/tb_setup_pass.png)

 - Click on the "**Continue**" button once you are done and verified everything is correct.

   ![](en/tb_setup_03.png)

**Thunderbird** now should auto-detect the needed settings like this:

![](en/tb_setup_04.png)

You can click on "**Configure manually**" to check the settings details.

![](en/tb_setup_details.png)

Click on **Done** once you are finished and... **\O/** Now you can manage your emails and interact with your calendars or tasks in a more fluid and efficient way through **Thunderbird**.

![](en/tb_setup_05.png)
