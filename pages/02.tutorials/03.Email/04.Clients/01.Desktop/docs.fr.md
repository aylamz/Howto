---
title: 'Clients de bureau'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - clients
        - bureau
page-toc:
    active: false
---

![](../thumb.png)

## Clients de messagerie multiplatformes
- [Thunderbird](thunderbird)
- [Claws Mail](claws-mail)


## GNU/Linux
- [Mutt](mutt)


## GNU/Linux: Intégration email du bureau
- [GNOME](gnome-desktop-integration)
- [KDE](kde-desktop-integration)
