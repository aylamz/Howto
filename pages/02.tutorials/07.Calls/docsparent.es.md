---
title: Llamadas
subtitle: Lo básico, configuraciones, clientes.
icon:  fa-microphone
published: true
taxonomy:
    category:
        - docs
        - topic
page-toc:
    active: false
---

# Llamadas de Disroot

![](calls.logo.png)

El servicio de **Llamadas de Disroot** es un programa de videoconferencias libre y de código abierto desarrollado por [**Jitsi**](https://jitsi.org/). Proporciona conferencias de audio y video en alta calidad con la cantidad de participantes que queramos. También permite transmitir nuestro escritorio o solo algunas ventanas a otras personas participantes de la llamada.

Se puede utilizar en móviles, tabletas, computadoras de escritorio o vía web y no requiere registrar una cuenta.

