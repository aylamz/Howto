---
title: Home
content:
    items:
      '@taxonomy':
        category: [topic]
    order:
        by: default
        dir: asc
    pagination: true
---

### Bem vindos ao site de guias de utilizador e tutoriais do Disroot.


O objetivo principal deste site é ajudar-te a orientares-te pelos vários serviços do **Disroot**.


Conseguir cobrir neste site todos os serviços e funcionalidades do **Disroot** para todos as plataformas e Sistemas Operativos. É um projeto bastante ambicioso e consome bastante tempo e requer bastante trabalho. Nós achamos que este trabalho é útil não apenas para os utilizadores do **Disroot** (disrooters), mas para toda a comunidade **Software Livre** e **Open Source** que utiliza o mesmo software, ou software parecido. Por causa disso, qualquer ajuda de utilizadores é sempre bem vinda e necessária.<br>

Por isso, se achas que falta um tutorial, que a informação não está correta ou que pode ser melhorada,
por favor entra em contacto connosco (ou melhor ainda) escreve um tutorial.<br>

Para veres as diferentes maneiras de que podes contribuir vê a seguinte [**página**](/contribute).
