---
title: Inicio
content:
    items:
      '@taxonomy':
        category: [topic]
    order:
        by: default
        dir: asc
    pagination: true
---

### Les damos la bienvenida al sitio de Guías y Tutoriales de Disroot.

El objetivo principal de este sitio es orientarles a través de los varios servicios de **Disroot**.

Aspiramos a cubrir todos los servicios, con todas las características provistas por **Disroot**, para todas las plataformas y/o Sistemas Operativos así como también para el mayor número posible de clientes de los dispositivos más ampliamente utilizados.

Es un proyecto muy ambicioso, que demanda mucho tiempo y requiere mucho trabajo. Pero como pensamos que puede ser beneficioso no solo para las personas usuarias de nuestros servicios (disrooters) sino para todas las comunidades de **Software Libre** y de **Código Abierto** que utilicen los mismos programas o similares, también pensamos que vale la pena. Por esto, cualquier ayuda es siempre necesaria y bienvenida.

Así que, si piensan que falta un tutorial, que la información no es correcta o puede mejorarse, por favor, contáctennos o (mejor aún) comiencen ustedes también a escribir una guía o tutorial.

Para conocer las diferentes maneras en que pueden contribuir, por favor, revisen esta [sección](/contribute).
