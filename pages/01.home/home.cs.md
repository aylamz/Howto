---
title: Domov
content:
    items:
      '@taxonomy':
        category: [topic]
    order:
        by: default
        dir: asc
    pagination: true
---

### Vítejte na stránkách Disroot's How-to.

Hlavním cílem těchto stránek je pomoci vám zorientovat se v různých službách **Disroot**.

Naším cílem je pokrýt všechny služby se všemi funkcemi, které **Disroot** poskytuje, pro všechny platformy a/nebo operační systémy a také co největší počet klientů pro nejpoužívanější zařízení.

Jedná se o velmi ambiciózní a časově náročný projekt, který vyžaduje mnoho práce. Protože si však myslíme, že by mohl být přínosný nejen pro naše uživatele (disrootery), ale i pro všechny komunity **Svobodného softwaru** a **Open Source** používající stejný nebo podobný software, myslíme si, že stojí za to. Z tohoto důvodu je jakákoli pomoc vždy potřebná a vítaná.

Pokud si tedy myslíte, že nějaký návod chybí, informace nejsou přesné nebo by se daly vylepšit, kontaktujte nás a (ještě lépe) začněte psát návod sami.

Chcete-li znát různé způsoby, jak můžete přispět, podívejte se do této [**sekce**](/cs/contribute).
