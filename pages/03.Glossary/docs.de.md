---
title: Glossar
published: false
visible:
taxonomy:
    category:
        - docs
    tags:
        - glossary
        - Glossar
page-toc:
    active: false
---

# Glossar (demnächst verfügbar)

- Git
- Pull request
- Server
- Instance
- Cloud
- GPDR
