---
title: 'Pokyny k obsahu'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - style
page-toc:
    active: true
---

# Pokyny k obsahu
Nepochybně existuje několik způsobů, jak napsat nebo přeložit průvodce. A pokud budeme hledat na internetu, najdeme nejrůznější styly: od velmi odborných až po některé velmi vtipné, projdeme přes některé docela nudné, některé vizuálně působivé a jiné určitě spartánské ve více než jednom ohledu. Všechny však mají jedno společné: **informace**. To je naše surovina.  

Cílem projektu **Howto** je poskytovat ne ledajaké informace, ale užitečné informace o softwaru dostupném na platformě **Disroot**, a to co nejsrozumitelnějším a nejpřístupnějším způsobem. Jak toho ale dosáhnout?

No, můžeme si položit několik otázek a zjistit, zda můžeme společně sestavit nějaké definice, které by nás v tomto úkolu vedly.
