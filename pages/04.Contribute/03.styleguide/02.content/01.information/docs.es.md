---
title: 'Sobre la información'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribuir
        - estilo
page-toc:
    active: true
---

# Construyendo los lineamientos sobre la información

## Primera pregunta: ¿Sobre qué podemos escribir?
Esta pregunta ya está parcialmente respondida en la página principal de este sitio:
> *"Aspiramos a cubrir todos los servicios, con todas las características provistas por **Disroot**, para todas las plataformas y/o Sistemas Operativos así como también para el mayor número posible de clientes de los dispositivos más ampliamente utilizados."*

Así que podemos escribir sobre los programas provistos por **Disroot**, cómo funcionan y que podemos hacer con ellos. Eso parece bastante evidente hasta que nos preguntamos si todas las personas usamos los mismos sistemas operativos en nuestras computadoras o dispositivos móviles.

El hecho de que nuestro foco esté puesto exclusivamente en el uso, fomento y promoción de **software libre y de código abierto** (y por lo tanto nuestras guías están escritas principalmente para GNU/Linux) no debería hacernos olvidar que hay muchas personas que eligen utilizar programas propietarios y su acercamiento a nuestros servicios no está relacionado con razones éticas o políticas sino pragmáticas.

Por eso decimos explícitamente:

> *"para todas las plataformas y/o Sistemas Operativos así como también para el mayor número posible de clientes de los dispositivos más ampliamente utilizados."*

Una lista básica de los principales sistemas operativos que intentamos cubrir sería la siguiente:

- **GNU/Linux**, **Microsoft Windows** y **Apple macOS** en computadoras de escritorio y laptops y
- **Google Android**, **Apple iOS**, **Jolla Sailfish OS** y **Microsoft Windows Mobile** en dispositivos móviles.

!! **Todas las personas, sin importar el sistema operativo que usen, pueden colaborar escribiendo cómo utilizar un servicio. Recuerden que lo más importante es hacer que la información esté disponible para cualquiera.**

## Segunda pregunta: ¿A quiénes están dirigidas las guías?
En principio, cualquier persona mayor de 16 años puede utilizar los servicios de **Disroot**. Pero podemos pensar también en el hecho de que todos y todas tenemos, por ejemplo, familiares y gente conocida a las que nos gustaría introducir en el uso de herramientas socialmente éticas y técnicamente útiles o quizás quieran hacerlo ellas mismas.

Así que cuando escribimos una guía debemos pensar que la información debe ser comprensible para cualquiera, independientemente de la edad o conocimientos técnicos. Por supuesto, siempre hay programas que son más complejos que otros, pero eso no debería significar que no hagamos el esfuerzo de intentar que sea posible para cualquier persona aprender a usarlos.

Esto nos lleva a la siguiente pregunta.

## Tercera pregunta: ¿Qué tipo de lenguaje deberíamos utilizar?
Para poder hacer las guías lo más accesibles posible, no solo para las personas usuarias sino también para quienes nos ayudan a traducirlas, debemos utilizar el Inglés o el Español de la manera menos informal que podamos. Esto significa evitar el uso de jerga técnica, lunfardo, o contracciones lingüísticas (por ejemplo, en Inglés, es preferible escribir "it will not" en lugar de "it won't"). También debemos tener en cuenta que estas guías podrían ser utilizadas o traducidas por otros proyectos que brinden servicios o programas similares y queremos que la tarea de adaptarlas consuma lo menos posible de su tiempo.

En aquellos casos donde el uso de términos técnicos sea inevitable, necesitaremos explicar brevemente qué significan o referir a una fuente externa que lo haga (p.ej., un artículo de la Wikipedia).

Sugerimos que, en la medida de lo posible, se realice la traducción de todos los términos que tengan su equivalente en nuestro idioma. Por ejemplo, la palabra "software" (programa) es de uso común dentro de las comunidades y entre personas de habla hispana vinculadas con la tecnología, pero recordemos que el objetivo es hacer accesible las guías para cualquiera y no debemos asumir que todas las personas saben qué significa.

## Cuarta pregunta: con respecto a las traducciones, ¿deberían ser literales?
Aunque no existe un solo método específico y efectivo, hay por lo menos dos cosas importantes que deberíamos tener en cuenta cuando hacemos una traducción.

1. **Entender el texto de la guía integralmente**. Antes de comenzar a traducir, es aconsejable leer el texto completo y "capturar" su esencia: "acerca de qué es", "cómo es presentada y/o estructurada la información", "es fácil/difícil de entender", etc. Esto nos ayudará a identificar el contexto de las palabras y expresiones que aparecen en un documento dado y del que depende su significado.

2. **Seguir el texto original es fundamental como también lo es preservar su fuidez**. A veces podemos encontrar palabras o expresiones que son realmente difíciles de traducir a nuestro idioma o incluso intraducibles. Así que necesitamos tener cuidado de no omitirlas o cambiar demasiado sus significados originales porque esto puede afectar el sentido general de una línea, un párrafo o un documento completo. Si tenemos dudas acerca de ciertas adaptaciones siempre podemos consultar o preguntar en la sala de chat de Howto.

Así que la literalidad de una traducción no siempre resulta en un texto coherente. Cuando es necesario realizar adaptaciones de expresiones o palabras, estas deben ser lo más fieles posible al sentido original del documento.

Finalmente, siempre necesitaremos revisar nuestras traducciones antes de enviarlas. Es muy común encontrar pequeños errores recurrentes o inconsistencias, y corregirlas a tiempo evitará que la traducción nos sea devuelta.

## Comentarios sobre las traducciones al español
Entendemos que el lenguaje no es neutral. En muchos países de habla hispana hay actualmente fuertes cuestionamientos al respecto y acciones concretas para modificar lo que muchas personas entendemos como una situación no representativa de nuestra diversidad como sociedad. Parece cada vez más notorio que la política y el lenguaje avanzan mucho más lentamente que nuestra vida y desarrollo social.

En español, tradicionalmente, las palabras se clasifican en dos géneros gramaticales: el femenino y el masculino, este último además cumple la función de representar lo genérico, o "neutro". Esta doble función, y su uso culturalmente extendido, del "masculino genérico" ha generado no pocos y muy interesantes y encendidos debates en diferentes ámbitos sociales que han ido creciendo a medida que las personas no binarias (aquellas que no se identifican o no se perciben como de género femenino o masculino) han ido ganando visibilidad y representación. También presenta un desafío para quienes asumimos la responsabilidad de escribir y traducir documentación. En ese sentido, nuestra posición como proyecto es clara: no podemos admitir el uso de formas del lenguaje que excluyan a nadie.

Tampoco podemos obligar a nadie a escribir de tal o cual manera, pero es necesario hacer algunas aclaraciones y comentarios respecto a la documentación y las prácticas lingüísticas que decidimos mantener.

Existen, básicamente, dos maneras de encarar el problema de la binariedad en nuestra lengua. Una es de tipo indirecta, que implica emplear estrategias lingüísticas y gramaticales que no denoten un género específico. Tomemos como ejemplo, la palabra inglesa "user". La forma indirecta del lenguaje no binario la traducirá como "la usuaria y/o usuario" o como "la persona usuaria". La forma directa del lenguaje no binario introduce el uso de neologismos y los llamados "neomorfemas" como la "e" o la "x". Siguiendo con el ejemplo de la palabra "user", su traducción sería, con este método directo, "le usuarie".

Admitimos cualquiera de las dos maneras, tanto para la traducción como para la creación de documentación. En el caso de la forma directa, sugerimos no utilizar el morfema "x" puesto que genera dificultades para las personas ciegas o con baja visión que utilizan lectores de texto.

Adicional y finalmente, es aconsejable tener presente que las guías en Inglés están escritas en el plural de segunda persona (you = ustedes / vosotres) y en el plural de la primera persona (we = nosotras/nosotres/nosotros).