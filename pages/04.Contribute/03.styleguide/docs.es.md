---
title: 'Lineamientos generales'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribuir
        - estilo
page-toc:
    active: true
---

# Guías de Estilo

![](h2_guide.jpg)

El propósito de esta sección es brindar algunos lineamientos generales para tener en cuenta cuando escribimos o traducimos un tutorial para el **[sitio How-to](https://howto.disroot.org) de Disroot** y de esa manera mantener una cierta coherencia en la estructura de la documentación, tanto visual como en términos de escritura.


---
# Contenidos
- ### [Estructura](structure)
Directorios y páginas

- ### [Contenido](content)
Escritura, traducción y contenido visual
