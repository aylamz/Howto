---
title: Cómo contribuir
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
page-toc:
    active: false
---

# Contribuir

![](contribute.jpg)

Pensamos que el conocimiento es una construcción colectiva: el resultado del trabajo en conjunto y cooperativamente, como una comunidad. Y ya sea que las contribuciones tomen la forma de una donación, escribiendo/traduciendo un tutorial o reportando errores, todas son esencialmente tiempo personal dedicado a otras personas. Un poco como el amor.

El principal objetivo de esta parte del proyecto **Disroot** es proveer información clara y accesible acerca de cómo utilizar y configurar los servicios que ofrecemos y, además, hacerlo en la mayor cantidad de idiomas posible. Consiste mayormente de guías y tutoriales que se desarrollan conforme los programas lo hacen. Por lo que siempre estamos buscando ayuda para revisar, escribir y traducir manuales.

Así que, para quienes quieran contribuir donando su tiempo y saberes, hemos intentado canalizar todos los esfuerzos a través de esta sección.

Aquí encontrarán información básica y guías para las diferentes maneras de contribuir, desde comentar hasta escribir un manual o traducirlos a sus idiomas.

---

# Contenido

## · [Procedimiento & Herramientas para las guías](procedure)
## · [Git: Configuración & preparación](git)
## · [Trabajando con editores de texto](git/editors)

## · [Lineamientos generales sobre el contenido](styleguide)
